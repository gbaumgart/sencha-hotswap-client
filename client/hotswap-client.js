var SockJS = window['SockJS'];
var LAST_EX;
var debug = false;
var host = "http://0.0.0.0";
var port = 19999;
var reconnect = true;
/*
const konst = v => () => v
const kTrue = konst(true)
const kFalse = konst(false)
const noop = () => {}
const ident = v => v
*/
//
// ─── OBJECT UTILS ───────────────────────────────────────────────────────────────
//
var GetSet = (function () {
    function GetSet() {
    }
    return GetSet;
}());
/**
 * @private
 * @param key
 * @returns {*}
 */
GetSet.getKey = function (key) {
    var intKey = parseInt(key, 10);
    if (intKey.toString() === key) {
        return intKey;
    }
    return key;
};
/**
 * internal set value at path in object
 * @private
 * @param obj
 * @param path
 * @param value
 * @param doNotReplace
 * @returns {*}
 */
GetSet.set = function (obj, path, value, doNotReplace) {
    if (Ext.isNumber(path)) {
        path = [path];
    }
    if (Ext.isEmpty(path)) {
        return obj;
    }
    if (Ext.isString(path)) {
        return GetSet.set(obj, path.split('.').map(GetSet.getKey), value, doNotReplace);
    }
    var currentPath = path[0];
    if (path.length === 1) {
        var oldVal = obj[currentPath];
        if (oldVal === void 0 || !doNotReplace) {
            obj[currentPath] = value;
        }
        return oldVal;
    }
    if (obj[currentPath] === void 0) {
        //check if we assume an array
        if (Ext.isNumber(path[1])) {
            obj[currentPath] = [];
        }
        else {
            obj[currentPath] = {};
        }
    }
    return GetSet.set(obj[currentPath], path.slice(1), value, doNotReplace);
};
/**
 * deletes an property by a path
 * @param obj
 * @param path
 * @returns {*}
 */
GetSet.del = function (obj, path) {
    if (Ext.isNumber(path)) {
        path = [path];
    }
    if (Ext.isEmpty(obj)) {
        return void 0;
    }
    if (Ext.isEmpty(path)) {
        return obj;
    }
    if (Ext.isString(path)) {
        return GetSet.del(obj, path.split('.'));
    }
    var currentPath = GetSet.getKey(path[0]);
    var oldVal = obj[currentPath];
    if (path.length === 1) {
        if (oldVal !== void 0) {
            if (Ext.isArray(obj)) {
                obj.splice(currentPath, 1);
            }
            else {
                delete obj[currentPath];
            }
        }
    }
    else {
        if (obj[currentPath] !== void 0) {
            return GetSet.del(obj[currentPath], path.slice(1));
        }
    }
    return obj;
};
/*
 * Private helper class
 * @private
 * @type {{}}
 */
var ObjectPath = (function () {
    function ObjectPath() {
    }
    return ObjectPath;
}());
ObjectPath.has = function (obj, path) {
    if (Ext.isEmpty(obj)) {
        return false;
    }
    if (Ext.isNumber(path)) {
        path = [path];
    }
    else if (Ext.isString(path)) {
        path = path.split('.');
    }
    if (Ext.isEmpty(path) || path.length === 0) {
        return false;
    }
    for (var i = 0; i < path.length; i++) {
        var j = path[i];
        if ((Ext.isObject(obj) || Ext.isArray(obj)) && Object.prototype.hasOwnProperty.call(obj, j)) {
            obj = obj[j];
        }
        else {
            return false;
        }
    }
    return true;
};
/**
 * Define private public 'ensure exists'
 * @param obj
 * @param path
 * @param value
 * @returns {*}
 */
ObjectPath.ensureExists = function (obj, path, value) {
    return GetSet.set(obj, path, value, true);
};
/**
 * Define private public 'set'
 * @param obj
 * @param path
 * @param value
 * @param doNotReplace
 * @returns {*}
 */
ObjectPath.set = function (obj, path, value, doNotReplace) {
    return GetSet.set(obj, path, value, doNotReplace);
};
/**
 Define private public 'insert'
 * @param obj
 * @param path
 * @param value
 * @param at
 */
ObjectPath.insert = function (obj, path, value, at) {
    var arr = ObjectPath.get(obj, path);
    at = ~~at;
    if (!Ext.isArray(arr)) {
        arr = [];
        ObjectPath.set(obj, path, arr);
    }
    arr.splice(at, 0, value);
};
/**
 * Define private public 'empty'
 * @param obj
 * @param path
 * @returns {*}
 */
ObjectPath.empty = function (obj, path) {
    if (Ext.isEmpty(path)) {
        return obj;
    }
    if (Ext.isEmpty(obj)) {
        return void 0;
    }
    var value, i;
    if (!(value = ObjectPath.get(obj, path))) {
        return obj;
    }
    if (Ext.isString(value)) {
        return ObjectPath.set(obj, path, '');
    }
    else if (Ext.isBoolean(value)) {
        return ObjectPath.set(obj, path, false);
    }
    else if (Ext.isNumber(value)) {
        return ObjectPath.set(obj, path, 0);
    }
    else if (Ext.isArray(value)) {
        value.length = 0;
    }
    else if (Ext.isObject(value)) {
        for (i in value) {
            if (Object.prototype.hasOwnProperty.call(value, i)) {
                delete value[i];
            }
        }
    }
    else {
        return ObjectPath.set(obj, path, null);
    }
};
/**
 * Define private public 'push'
 * @param obj
 * @param path
 */
ObjectPath.push = function (obj, path /*, values */) {
    var arr = ObjectPath.get(obj, path);
    if (!Ext.isArray(arr)) {
        arr = [];
        ObjectPath.set(obj, path, arr);
    }
    arr.push.apply(arr, Array.prototype.slice.call(arguments, 2));
};
/**
 * Define private public 'coalesce'
 * @param obj
 * @param paths
 * @param defaultValue
 * @returns {*}
 */
ObjectPath.coalesce = function (obj, paths, defaultValue) {
    var value;
    for (var i = 0, len = paths.length; i < len; i++) {
        if ((value = ObjectPath.get(obj, paths[i])) !== void 0) {
            return value;
        }
    }
    return defaultValue;
};
/**
 * Define private public 'get'
 * @param obj
 * @param path
 * @param defaultValue
 * @returns {*}
 */
ObjectPath.get = function (obj, path, defaultValue) {
    if (Ext.isNumber(path)) {
        path = [path];
    }
    if (Ext.isEmpty(path)) {
        return obj;
    }
    if (Ext.isEmpty(obj)) {
        //Ext doesnt seem to work with html nodes
        if (obj && obj.innerHTML === null) {
            return defaultValue;
        }
    }
    if (Ext.isString(path)) {
        return ObjectPath.get(obj, path.split('.'), defaultValue);
    }
    var currentPath = GetSet.getKey(path[0]);
    if (path.length === 1) {
        if (obj && obj[currentPath] === void 0) {
            return defaultValue;
        }
        if (obj) {
            return obj[currentPath];
        }
    }
    if (!obj) {
        return defaultValue;
    }
    return ObjectPath.get(obj[currentPath], path.slice(1), defaultValue);
};
/**
 * Define private public 'del'
 * @param obj
 * @param path
 * @returns {*}
 */
ObjectPath.del = function (obj, path) {
    return ObjectPath.del(obj, path);
};
//
// ─── ExtJs Helpers ───────────────────────────────────────────────────────────
//
var GetExtClass = function (className) {
    return ObjectPath.get(window, className, null);
};
var GetExtClassPrototype = function (className) {
    var clazz = GetExtClass(className);
    if (clazz) {
        return clazz.prototype;
    }
};
var InspectExtClass = function (className) {
    var proto = GetExtClass(className);
    if (proto) {
        proto = proto.prototype;
        for (var p in proto) {
            if (typeof proto[p] !== 'function') {
                console.log(p, proto[p]);
            }
        }
    }
};
var InspectExtClassEx = function (proto) {
    if (proto) {
        proto = proto.prototype;
        for (var p in proto) {
            if (typeof proto[p] !== 'function') {
                console.log(p, proto[p]);
            }
        }
    }
};
var MergeClass = function (oldClass, newClass) {
    for (var p in newClass) {
        if (typeof newClass[p] === 'function') {
            if (oldClass[p]) {
                oldClass[p] = newClass[p];
            }
            else {
                console.error('cant update ' + p);
            }
        }
    }
};
var UpdateExtJSClass = function (oldClass, newClass, clazz) {
    Ext.GlobalEvents.doFireEvent('testx', [{
            clazz: clazz,
            module: newClass,
            update: MergeClass
        }]);
};
//
// ─── APPLICATION CODE ───────────────────────────────────────────────────────────
//
var RegisterHotswap = function (who) {
    var clazz = who.$className;
    var self = who;
    Ext.GlobalEvents.on('testx', function (msg) {
        if (msg.clazz === clazz) {
            console.log('updated ' + clazz);
            msg.update(self, msg.module);
        }
    });
};
var InFileChanged = function (path, clazz, content) {
    if (!path.match(/\.(js)$/i)) {
        console.warn('Ext.JS Hotswap: is not a Javascript file ' + path + '');
        return;
    }
    var module = GetExtClassPrototype(clazz);
    if (module) {
        console.log('Ext.JS Hotswap: Class changed : ' + clazz);
        Ext['undefine'](clazz);
        var newClass = eval(content);
        //InspectExtClassEx(newClass);
        LAST_EX = newClass;
        UpdateExtJSClass(module, newClass.prototype, clazz);
    }
    else {
        console.error('Ext.JS Hotswap Error: cant find class ' + clazz);
    }
};
var InitHotSwap = function () {
    var protocol = [
        'websocket',
        'xdr-streaming',
        'xhr-streaming',
        'iframe-eventsource',
        'iframe-htmlfile',
        'xdr-polling',
        'xhr-polling',
        'iframe-xhr-polling',
        'jsonp-polling'
    ];
    var options = {
        debug: 0,
        devel: false,
        noCredentials: true,
        nullOrigin: true,
        transports: null
    };
    options.transports = protocol;
    var ready = function () { return console.info('Connected to Ext.JS Hotswap server'); };
    var sock = new SockJS(host + ":" + port, null, options);
    sock.onopen = function () { return ready; };
    sock.onmessage = function (msg) {
        var message = JSON.parse(msg.data);
        var type = message.event;
        var data = message.data;
        debug && console.info('Ext.JS Hotswap server message : ' + type, data);
        if (type === 'FileChanged') {
            console.info('File changed : ' + data.class + ' @ ' + data.path);
            InFileChanged(data.path, data.class, data.content);
        }
    };
    sock.onerror = function () { return debug && console.error('on sockjs error'); };
    sock.onclose = function (e) {
        if (reconnect) {
            debug && console.log('closed ' + host + ' try re-connect');
            setTimeout(function () {
                InitHotSwap();
            }, 2000);
        }
        else {
            debug && console.log('closed ' + host, e);
        }
    };
};
setTimeout(function () {
    Ext.onReady(function () {
        InitHotSwap();
    });
}, 2000);
//# sourceMappingURL=hotswap-client.js.map